# Libft - @WeThinkCode_

My implementation of some of the Standard C Library functions including some additional ones.

### What is libft?
[Libft](libft.en.pdf) is an individual project at WeThinkCode_ that requires us to re-create some standard C library functions including some additional ones that can be used later to build a library of useful functions for the rest of the program.

Disclaimer: *Reinventing the wheel is bad, Wethinkcode_ makes us do this just so we can have a deeper understanding of data structures and basic algorithms. At WeThinkCode_ we're not allowed to use some standard libraries on our projects, so we have to keep growing this library with our own functions as we go farther in the program.*


### What's in it?

There are 4 sections:

1.  **Libc Functions:** Some of the standard C functions
2.  **Additional functions:** Functions WeThinkCode_ deems will be useful for later projects
3.  **Bonus Functions:** Functions WeThinkCode_ deems will be useful for linked list manipulation
4.  **Personal Functions:** Functions I believe will be useful later.


Libc functions | Additional functions | Bonus Functions | Personal Functions
:----------- | :-----------: | :-----------: | -----------:
memset		| ft_memalloc	| ft_lstnew		| ft_abs.c 
bzero		| ft_memdel		| ft_lstdelone	| ft_int_len.c 
memcpy		| ft_strnew		| ft_lstdel		| ft_strrev.c   
memccpy		| ft_strdel		| ft_lstadd		|     
memmove		| ft_strclr		| ft_lstiter	|     
memchr		| ft_striter	| ft_lstmap		| 
memcmp		| ft_striteri	|				| 
strlen		| ft_strmap		|				| 
strdup		| ft_strmapi	|				| 
strcpy		| ft_strequ		|				| 
strncpy		| ft_strnequ	|			| 
strcat		| ft_strsub		| | 
strlcat		| ft_strjoin	| | 
strchr		| ft_strtrim	| | 
strrchr		| ft_strsplit	| | 
strstr		| ft_itoa		| | 
strnstr		| ft_putchar	| | 
strcmp		| ft_putstr		| | 
strncmp		| ft_putendl	| | 
atoi		| ft_putnbr		| | 
isalpha		| ft_putchar_fd	| | 
isdigit		| ft_putstr_fd	| | 
isalnum		| ft_putendl_fd	| | 
isascii		| ft_putnbr_fd	| | 
isprint		|| |
toupper		| | |
tolower		| | |

Notes:

- Most of the the files and function names are namespaced with an **ft** in front. It stands for Fourty Two
- The project instructions require that we put all the source files in the root directory.

My code is not the best, but it passed all the WeThinkCode_ tests successfully.

### How does it work?

The goal is to create a library called libft.a from the source files so I can later use that library from other projects at WeThinkCode_.

To create that library, after downloading/cloning this project, **cd** into the project, and run make.
You should see a *libft.a* file and some object files (.o).

Now to clean up (removing the .o files from the root), call `make clean`

### How do I use the library?

I added an example file called **example.c**, it's using the function **ft_putstr** to print "DON'T PANIC" to the screen. 

If you try to compile it with gcc using `gcc example.c` you will get an *undefined symbol* error for ft_putstr. 

You have to tell the file where your library resides and which library it is using:

`gcc example.c -L. -lft`

-L takes the path to your library. `.` in this case<br>
-l takes the name of your library. This is the set of characters that come after `lib` in your library name.

That's it. Now run it using `./a.out`
